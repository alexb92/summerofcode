{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lesson 3 -  Notorious OOP"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# Imports\n",
    "from random import randint\n",
    "import time\n",
    "from math import factorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The core concept of object-oriented programming is pretty self-explanatory: it is focused on objects. These objects are modeled to resemble the real-life conditions the software is designed for. Commonly, objects refer to themselves as `this` or `self`.\n",
    "\n",
    "Let's take a school management program as an example. Here we have multiple entities with different roles, that ideally should be reflected in the software. In theory, you could also just have a single type for all roles and entities and only differentiate via attributes. This approach, however, is very naive and usually too simple for real applications.\n",
    "\n",
    "Let's take a look at a teacher class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# Teachers\n",
    "class Teacher:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every object in our code instantiated from this class will now be declared as a Teacher and can be differentiated from other classes such as Student very easily.\n",
    "\n",
    "Of course, different roles need to have different abilities and responsibilities as well. These will be reflected in our code as class functions. Class functions are functions that only an object of this certain type can perform.\n",
    "\n",
    "Let's extend our Teacher class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "class Teacher:\n",
    "    def grade_homework(self) -> int:\n",
    "        return randint(1,6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So if we now instantiate a teacher object, we can use it to call the grade_homework function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "t1 = Teacher()\n",
    "\n",
    "t1.grade_homework()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we try to call the function in the global scope, it will raise an error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "grade_homework()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So let's take a look at the syntax of our function definition. As you could clearly see, we did not need any argument when we called it as a class function of t1. If we look at the definition of the class, however, we can see that we have defined the input parameter `self`. How does this work?\n",
    "\n",
    "As stated in the beginning, classes refer to themselves as `self`. With only a few exceptions, all class functions have to do this. This statement makes sure, that an object of the class has been instantiated correctly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Class Properties\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Properties are the public getter-functions of classes in Python. Whenever a class attribute should not be accessed directly, properties should be used. They have to be declared via a specific decorator.\n",
    "\n",
    "Properties go hand in hand with a certain feature in Python that mimics the behaviour of the public-private principle in e.g. C++. While Python does not outright support private variables and functions, it is possible to mark them to obscure them from being easy to access and to signal to other programmers that these functions can be considered private.\n",
    "\n",
    "By adding two underscore to the beginning of the name of a variable/function, most modern IDEs will automatically consider it a private function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Teacher:\n",
    "    __id = \"897GG\"\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id\n",
    "    \n",
    "    def grade_homework(self) -> int:\n",
    "        return randint(1,6)\n",
    "    \n",
    "t = Teacher()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to access id."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t.id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try it with directly with __id."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t.__id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can observe, it is significantly harder to access the attribute __id without using the property function. There are still ways, however, to get this attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t._Teacher__id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And with this we know how Python mimics private methods: by name wrangling. If we assign the double underscores to any attribute or function, Python automatically renames it to _{class_name}{attribute_name}, therefore _Teacher__id is a valid call."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Class Methods\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Class methods are methods that can be called WITHOUT instantiating an object of a particular class. They also do not refer to themselves as `self` but to their own class as `cls`. To declare a function a class method, we have to use a specific decorator.\n",
    "\n",
    "Let's take a look at an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Teacher:\n",
    "    __id = \"897GG\"\n",
    "    subject = \"Informatics\"\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id\n",
    "    \n",
    "    @classmethod\n",
    "    def state_subject(cls):\n",
    "        return cls.subject\n",
    "    \n",
    "    def grade_homework(self) -> int:\n",
    "        return randint(1,6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Teacher.state_subject()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Class methods return values of variables at the state of class definition. This may sound ominous, so let's take a look at an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = Teacher()\n",
    "t.subject = \"Math\"\n",
    "t.subject"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t.state_subject()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Static Methods\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Static methods, like class methods, can be called WITHOUT instantiating an object. The difference between class and static methods is, that a class method knows of the class itself, a static method is basically a global function that is assigned to a specific class. It has no knowledge of the attributes of the class it is assigned to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Teacher:\n",
    "    __id = \"897GG\"\n",
    "    subject = \"Informatics\"\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id\n",
    "    \n",
    "    @classmethod\n",
    "    def state_subject(cls):\n",
    "        return cls.subject\n",
    "    \n",
    "    @staticmethod\n",
    "    def grade_homework() -> int:\n",
    "        return randint(1,6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, we have now defined the grade_homework function as a static method. We can do this since the function itself does not use any of the class attributes.\n",
    "\n",
    "Let's try and run it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Teacher.grade_homework()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Double-underscore (dunder) methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Dunder methods are very powerful builtin methods in Python's approach to OOP. There is a whole list of different methods and when they are called.\n",
    "\n",
    "These methods are used as a simplified API for programmers to use basic commands. If we use e.g. len() the function then calls the dunder method `__len__`, which runs the implementation for this particular class.\n",
    "\n",
    "Since every dunder method has a completely different function, we will only take a look at 2 common examples: `__init__` and `__repr__`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# __init__\n",
    "class Teacher:\n",
    "    __id = \"897GG\"\n",
    "    subject = \"Informatics\"\n",
    "    \n",
    "    def __init__(self, first_name: str, last_name: str):\n",
    "        self.fn = first_name\n",
    "        self.ln = last_name\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id\n",
    "    \n",
    "    @classmethod\n",
    "    def state_subject(cls):\n",
    "        return cls.subject\n",
    "    \n",
    "    @staticmethod\n",
    "    def grade_homework() -> int:\n",
    "        return randint(1,6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now try to instantiate the class like before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = Teacher()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, it does not work. This is due to the two input parameters that we have added to the `__init__` function. This dunder method is always called, when an object is created. This also means, that every input parameter we put into the `__init__` function, is a required parameter when instantiating the object.\n",
    "\n",
    "Let's try that again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = Teacher(\"Alex\", \"Benkö\")\n",
    "\n",
    "t.ln"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now add `__repr__`, the dunder method that is called whenever we want to print an object. First let's see what kind of output we get if we want to print the Teacher object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doesn't look too great... We also can't see any more information other than that it is an object of type Teacher. Let's improve that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# __repr__\n",
    "class Teacher:\n",
    "    __id = \"897GG\"\n",
    "    subject = \"Informatics\"\n",
    "    \n",
    "    def __init__(self, first_name: str, last_name: str):\n",
    "        self.fn = first_name\n",
    "        self.ln = last_name\n",
    "    \n",
    "    def __repr__(self):\n",
    "        return f\"{self.fn} {self.ln}\"\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id\n",
    "    \n",
    "    @classmethod\n",
    "    def state_subject(cls):\n",
    "        return cls.subject\n",
    "    \n",
    "    @staticmethod\n",
    "    def grade_homework() -> int:\n",
    "        return randint(1,6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = Teacher(\"Alex\", \"Benkö\")\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inheritance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Inheritance is a central theme in OOP. You can think of it like Lego, where you can implement a base block that all other blocks are built upon. Whenever a class inherits from another, it inherits all attributes and functions. If we want to redefine a function that is inherited from the parent to the child class, we can simply overwrite it.\n",
    "\n",
    "Let's add an intelligent parent class for our Teacher class to inherit from."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SchoolStaff:\n",
    "    __id = \"897GG\"\n",
    "    \n",
    "    def __init__(self, first_name: str, last_name: str):\n",
    "        self.fn = first_name\n",
    "        self.ln = last_name\n",
    "    \n",
    "    def __repr__(self):\n",
    "        return f\"{self.fn} {self.ln}\"\n",
    "    \n",
    "    @property\n",
    "    def id(self):\n",
    "        return self.__id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have to declare Teacher a child class of SchoolStaff. Since we already have the ID, first and last name defined here, we don't have to necessarily overwrite them in our Teacher class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Teacher(SchoolStaff):\n",
    "    subject = \"Informatics\"\n",
    "   \n",
    "    @classmethod\n",
    "    def state_subject(cls):\n",
    "        return cls.subject\n",
    "    \n",
    "    @staticmethod\n",
    "    def grade_homework() -> int:\n",
    "        return randint(1,6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = Teacher(\"Alex\", \"Benkö\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we have our parent class SchoolStaff, we can also use it to define a new class for the principal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Principal(SchoolStaff):\n",
    "   \n",
    "   def hire_teacher(self, first_name: str, last_name: str):\n",
    "        return Teacher(first_name, last_name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = Principal(\"Michael\", \"Maurer\")\n",
    "t2 = p.hire_teacher(\"Nu\", \"Dewd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Overwriting Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now take a look at how to overwrite parent functions within a child class. Let's assume we have a new class NewTeacher. New teachers are ususally highly motivated, meaning they'll actually read a student's homework before grading it. We want to reflect that in our code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class NewTeacher(Teacher):\n",
    "    \n",
    "    def grade_homework(self, quality_of_work: int):\n",
    "        mapping = {1: (90,100), 2: (80,89), 3: (70,79), 4: (60,69), 5: (0,59)}\n",
    "        \n",
    "        return next(k for k,v in mapping.items() if quality_of_work in range(v[0], v[1]+1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_new = NewTeacher(\"Alex\", \"Benkö\")\n",
    "t_new.grade_homework(80)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And just like that, we have overwritten the parent function with our own implementation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wrappers & Decorators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Wrappers are a pattern that are usually used in conjunction with complex functions to add a pro- or epilogue. This is called metaprogramming. We are not changing a function directly, we're rather defining another function that wraps around and might use the output in a certain way.\n",
    "\n",
    "There is virtually no difference in defining wrappers and decorators. Decorators are wrappers that are added onto the function definition with the @ symbol while wrappers are added when writing the call to the function. Decorators usually return the return value of the wrapped function, wrappers return a wrapped function.\n",
    "\n",
    "Let's define a simple wrapper function and use it in both ways."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def time_this(func):\n",
    "    \n",
    "    def inner(*args, **kwargs):\n",
    "        start = time.time()\n",
    "        result = func(*args, **kwargs)\n",
    "        end = time.time()\n",
    "\n",
    "        print(func.__name__, end-start)\n",
    "        return result\n",
    "    \n",
    "    return inner"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have now defined a very simple method to time the execution time of function calls. Let's put it to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function to be wrapped\n",
    "def wrap_me():\n",
    "    return [y for i in range(50) if (y := factorial(i)) > 40000]\n",
    "\n",
    "# Wrap\n",
    "wrapped_function = time_this(wrap_me)\n",
    "wrapped_function()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function to be wrapped\n",
    "# Decorate\n",
    "@time_this\n",
    "def wrap_me():\n",
    "    return [y for i in range(50) if (y := factorial(i)) > 40000]\n",
    "\n",
    "wrap_me()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises - OOP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Write a parent class and 2 child classes that each further define functions and attributes. Use as many of the principles we have learned in this lesson as possible and apply the documentation standards of lesson 1!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises - REST API"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Rosalind Bioinformatics Problems](http://rosalind.info/problems/list-view/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 1 - Counting DNA Nucleotides"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2 - Transcribing DNA into RNA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3 - Complementing a Strand of DNA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 4 - Computing CG-Content"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 5 - Counting Point Mutations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}