from typing import Optional
from pydantic import BaseModel, Field, validator

## Pydantic
"""As is well known, Python is not a statically typed programming language, meaning there are not built-in restrictions
prohibiting a user from passing arguments of a wrong type. The output may be wrong or an error may be raised, but other
than that, a user would need to implement their own type checks to avoid type errors.

In order to avoid to have to manually implement type checks on every class/ function, the package pydantic can be used.
pydantic enforces type hints at runtime, and provides user friendly errors when data is invalid. But it does not only
implement type checks, it also features easy-to-use data validation functions that can be implemented on each individual
input parameter."""

# Simple Class
class SimpleClass(BaseModel):
	my_var: str = Field(description="Some variable of type string",
	                    example="This is an example text")

# This works
sc = SimpleClass(my_var="test")
print(sc)

# # This doesn't work
# sc2 = SimpleClass(my_var=["test"])
# print(sc2)


# Complex Class
class ComplexClass(BaseModel):
	v1: str = Field(description="A string",
	                example="test")
	v2: int = Field(description="An integer",
	                example=10,
	                default=10)
	v3: list = Field(description="A list",
	                 default_factory=list)

	@validator("v2")
	def validate_v2(cls, value):
		if value not in range(0, 21):
			raise ValueError("v2 must be within 0-20")
		return value

	@validator("v3")
	def validate_v3(cls, value):
		assert all(isinstance(i, str) for i in value), "all list elements must be strings"
		return value

cc = ComplexClass(v1="test", v2=20, v3=["test", "i"])
print(cc)