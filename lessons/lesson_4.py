from dataclasses import dataclass, field
from typing import Union, Optional, Any


## Dataclasses
"""Dataclasses are a way of automating certain dunder methods of a class. Usually, things like __init__ and __repr__ need 
to be defined separately, but by using the dataclass decorator, these functions are added instantly.

Obviously, these can also be overwritten afterwards."""


class NewClass:
	def __init__(self, value_1: str, value_2: int, value_3: float, value_4: list = []):
		self.value_1 = value_1
		self.value_2 = value_2
		self.value_3 = value_3
		self.value_4 = value_4

	def add_to_value_4(self):
		self.value_4.append("TEST")


@dataclass
class NewDataClass:
	value_1: str
	value_2: Union[int, float]
	value_3: Optional[float] = 2.5
	value_4: Optional[Any] = "anything at all"
	value_5: list = field(default_factory=list)

	def __add__(self, other):
		if isinstance(other, NewDataClass):
			return NewDataClass(self.value_1 + other.value_1, self.value_2 + other.value_2,
			                    self.value_3 + other.value_3)
		raise TypeError("Can only add NewDataClass to NewDataClass")

	def __iadd__(self, other):
		if isinstance(other, NewDataClass):
			self.value_1 += other.value_1
			self.value_2 += other.value_2
			self.value_3 += other.value_3
			return self
		raise TypeError("Can only add NewDataClass to NewDataClass")


def main():
	c1 = NewDataClass("test", 10, 20.5)
	c2 = NewClass("test", 10, 20.5)
	c3 = NewDataClass("test", 10, 20.5)
	c4 = NewClass("test", 10, 20.5)

	print(c1)
	print(c2)
	print(c1 == c3)
	print(c2 == c4)
	print(c1 + c3)

if __name__ == '__main__':
	main()