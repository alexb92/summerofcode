from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from fastapi.exceptions import HTTPException
from models import DnaSequence, RnaSequence

from typing import Union

app = FastAPI()


@app.get('/', include_in_schema=False)
async def index():
	return RedirectResponse(url="/docs")

@app.post("/analyze", tags=["DNA", "RNA"], name="analyze_sequence", operation_id="analyze_sequence")
def analyze_sequence(sequence: Union[DnaSequence, RnaSequence]):
	return sequence.analyze()

@app.post("/transcribe", tags=["DNA", "RNA"], name="transcribe_sequence", operation_id="transcribe_sequence")
def transcribe_sequence(sequence: Union[DnaSequence, RnaSequence]):
	return sequence.transcribe()

@app.post("/complement", tags=["DNA", "RNA"], name="complement_sequence", operation_id="complement_sequence")
def complement_sequence(sequence: Union[DnaSequence, RnaSequence]):
	return sequence.complement()

@app.post("/translate", tags=["DNA", "RNA"], name="translate_sequence", operation_id="translate_sequence")
def translate_sequence(sequence: RnaSequence):
	try:
		t_seq = sequence.translate()
	except (TypeError, ValueError) as e:
		return HTTPException(status_code=401, detail=str(e))
	return t_seq

@app.post("/point_mutations", tags=["DNA", "RNA"], name="count_point_mutations", operation_id="count_point_mutations")
def count_point_mutations(sequence: Union[DnaSequence, RnaSequence], other_sequence: Union[DnaSequence, RnaSequence]):
	try:
		pm = sequence.count_point_mutations(other_sequence=other_sequence)
	except TypeError as e:
		return HTTPException(status_code=401, detail=str(e))
	return pm
