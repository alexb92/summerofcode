from soc_client import DNAApi, RNAApi, RnaSequence, DnaSequence, ApiClient, Configuration


class Client:
	def __init__(self, host="http://localhost:8000"):
		config = Configuration(host=host)
		client = ApiClient(configuration=config)

		self.dna_api = DNAApi(client)
		self.rna_api = RNAApi(client)

		print(self.dna_api.analyze_sequence(DnaSequence(sequence="ACAGTCGATCGTA")))
		print(self.rna_api.analyze_sequence(RnaSequence(sequence="AUCGAUCGAUCUG")))

Client()