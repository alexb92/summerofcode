import re
from collections import Counter
from difflib import SequenceMatcher
from typing import Union


# DNA: Counting DNA Nucleotides
def count_nucleotides(sequence: str) -> dict[str, int]:
	"""
	Function counts and returns amount of each nucleotide.

	Args:
		sequence (str): DNA or RNA sequence

	Returns:
		dict[str, int]: Amount per nucleotide
	"""
	return dict(Counter(sequence))


# RNA: Transcribing DNA into RNA
def transcribe_dna_to_rna(sequence: str) -> str:
	"""
	Function transcribes DNA to RNA.

	Args:
		sequence (str): DNA sequence

	Returns:
		str: RNA sequence
	"""
	return sequence.replace("T", "U")


# REVC: Complementing a Strand of DNA
def complement_dna(sequence: str) -> str:
	"""
	Function complements single strand of DNA.

	Args:
		sequence (str): DNA sequence

	Returns:
		str: DNA sequence
	"""
	mapping = {"A": "T", "T": "A", "G": "C", "C": "G"}
	return ''.join([mapping[i] for i in sequence])


# GC: Computing GC Content
def calculate_gc_content(sequence: str) -> float:
	"""
	Function calculates GC-content of nucleotide sequence.

	Args:
		sequence (str): DNA or RNA sequence

	Returns:
		float: % of G & C in sequence
	"""
	return round(len([i for i in sequence if i in ["G", "C"]]) / len(sequence), 2) * 100


# HAMM: Counting Point Mutations
def count_point_mutations(sequence: str, other_sequence: str):
	"""
	Function counts point mutations between two sequences.

	Args:
		sequence (str): DNA or RNA sequence
		other_sequence (str): DNA or RNA sequence

	Returns:
		int: amount of differences between sequences
	"""
	return len([i for nr, i in enumerate(sequence) if i != other_sequence[nr]])


# PROT: Translating RNA into Protein
def rna_to_protein(sequence: str) -> str:
	"""
	Maps given RNA sequence to protein short form.

	Args:
		sequence (str): RNA sequence

	Returns:
		str: Protein sequence
	"""
	mapping = {'M': ['AUG'],
	           'W': ['UGG'],
	           'Y': ['UAU', 'UAC'],
	           'F': ['UUU', 'UUC'],
	           'C': ['UGU', 'UGC'],
	           'N': ['AAU', 'AAC'],
	           'D': ['GAU', 'GAC'],
	           'Q': ['CAA', 'CAG'],
	           'E': ['GAA', 'GAG'],
	           'H': ['CAU', 'CAC'],
	           'K': ['AAA', 'AAG'],
	           'I': ['AUU', 'AUC', 'AUA'],
	           'G': ['GGU', 'GGC', 'GGA', 'GGG'],
	           'A': ['GCU', 'GCC', 'GCA', 'GCG'],
	           'V': ['GUU', 'GUC', 'GUA', 'GUG'],
	           'T': ['ACU', 'ACC', 'ACA', 'ACG'],
	           'P': ['CCU', 'CCC', 'CCA', 'CCG'],
	           'L': ['CUU', 'CUC', 'CUA', 'CUG', 'UUA', 'UUG'],
	           'S': ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC'],
	           'R': ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'],
	           '<': ['UAA', 'UAG', 'UGA']}

	# Quality check
	if len(sequence) % 3:
		raise ValueError("Sequence must be divisible by 3!")

	# Split into pairs of 3
	codons = [sequence[i:i + 3] for i in range(0, len(sequence), 3)]

	return ''.join([k for x in codons for k, v in mapping.items() if x in v])


# SUBS: Finding a Motif in DNA
def find_dna_motif(sequence: str, substring: str) -> list[int]:
	"""
	Finds substring of original sequence and returns the positions of occurrence.

	Args:
		sequence (str): DNA sequence
		substring (str): Substring of DNA sequence

	Returns:
		list[int]: positions of occurrence
	"""
	return [m.start() for m in re.finditer(substring, sequence)]


# CONS: Consensus and Profile
def calculate_consensus(sequences: list[str]) -> Union[dict[str, int], str]:
	"""
	Calculates a consensus sequence out of all given sequences. Input sequences have to be of same length.

	Args:
		sequences (list[str]): list of same length sequences

	Returns:
		dict[str, int]: dictionary containing the nucleotides and their summed up occurrences
		str: the consensus sequence
	"""
	# Quality check
	if not all(len(sequences[0]) for i in sequences):
		raise ValueError("Sequences have to be all of same length")

	# Building up a dictionary and appending occurrences
	occurrences = {x: [[] for _ in range(len(i))] for i in sequences for x in i}
	[occurrences[x][nr].append(1) for i in sequences for nr, x in enumerate(i)]

	# Summing up occurrences
	summing_up = lambda x: [sum(i) for i in x]
	occurrences = {k: summing_up(v) for k, v in occurrences.items()}

	# Extracting consensus sequences
	consensus = [[]]
	for i in range(len(occurrences["A"])):
		d = {x: occurrences[x][i] for x in [*occurrences]}
		l = [i for i in [*occurrences] if d[i] == max(d.values())]
		consensus = [x + [i] for i in l for x in consensus]

	consensus = [''.join(i) for i in consensus]

	return occurrences, consensus


# LCSM: Finding a Shared Motif
def find_shared_motif(sequence: str, other_sequence: str) -> str:
	"""
	Compares two sequences and finds longest match between them.

	Args:
		sequence (str): DNA sequence
		other_sequence: DNA sequence

	Returns:
		str: longest match
	"""
	match = SequenceMatcher(None, sequence, other_sequence).find_longest_match(alo=0, blo=0)
	return sequence[match.a: match.a + match.size]


def main():
	# Sequences
	dna_sequence = "ACGTGTGACCATGGGTTACGA"
	other_dna_sequence = "GCGTATGACTATCGGATAGGA"
	rna_sequence = transcribe_dna_to_rna(dna_sequence)

	print(calculate_gc_content(dna_sequence))
	print(complement_dna(dna_sequence))
	print(count_nucleotides(dna_sequence))
	print(count_point_mutations(dna_sequence, other_dna_sequence))
	print(rna_to_protein(rna_sequence))
	print(find_dna_motif(dna_sequence, "ACG"))
	print(calculate_consensus(["ACGTGTGACCATGGGTTACGA",
	                           "ACGTGTGATAATTGGTTACGA",
	                           "ACGTGTGATAATTGGTTACGA",
	                           "ACGTGTGATAGTGGGGTACGA",
	                           "ACGTGTGACCATGGGGTACGA",
	                           "ACGTGTGACCATGGGGTACGA"]))
	print(find_shared_motif(dna_sequence, other_dna_sequence))


if __name__ == '__main__':
	main()
