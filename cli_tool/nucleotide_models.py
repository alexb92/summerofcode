from abc import ABC, abstractmethod
from collections import Counter
from typing import Union, ForwardRef

from Bio.Seq import Seq
from Bio.SeqUtils import MeltingTemp as mt
from pydantic import BaseModel, Field, validator

# Forward reference to be able to correctly type certain input
DnaSequence = ForwardRef('DnaSequence')
RnaSequence = ForwardRef('RnaSequence')


class NucleotideSequence(BaseModel, ABC):
	sequence: str = Field(description="A nucleotide sequence (RNA/ DNA)",
	                      example="ACCTTGGCTCGAA")

	def __add__(self, other):
		if not isinstance(other, self.__class__):
			raise TypeError("Can only add two sequences of the same type")
		return self.sequence + other.sequence

	def __iadd__(self, other):
		if not isinstance(other, self.__class__):
			raise TypeError("Can only add two sequences of the same type")
		self.sequence += other.sequence
		return self

	@property
	def length(self) -> int:
		return len(self.sequence)

	@property
	def melting_temperature(self) -> float:
		return round(mt.Tm_NN(Seq(self.sequence)), 2)

	@property
	def gc_content(self) -> float:
		gcc = round(len([i for i in self.sequence if i in ["G", "C"]]) / len(self.sequence), 2) * 100
		return gcc

	@abstractmethod
	def complement(self) -> str:
		pass

	@abstractmethod
	def transcribe(self, **kwargs) -> str:
		pass

	@abstractmethod
	def translate(self) -> str:
		pass

	def count_nucleotides(self) -> dict[str, int]:
		"""
		Function counts and returns amount of each nucleotide.

		Returns:
			dict[str, int]: Amount per nucleotide
		"""
		return dict(Counter(self.sequence))

	def analyze(self):
		return {'Type': self.sequence_type,
		        'Sequence': self.sequence,
		        'Length': self.length,
		        'Melting Temperature': self.melting_temperature,
		        'GC Content in %': self.gc_content}

	def count_point_mutations(self, other_sequence: Union[DnaSequence, RnaSequence]) -> int:
		"""
		Function counts point mutations between two sequences.

		Args:
			other_sequence (str): DNA or RNA sequence

		Returns:
			int: amount of differences between sequences
		"""
		if not isinstance(other_sequence, self.__class__):
			raise TypeError("Must be of same sequence type")

		if not other_sequence.length == self.length:
			raise ValueError("Sequences should be exact same length")
		return len([i for nr, i in enumerate(self.sequence) if i != other_sequence.sequence[nr]])


class DnaSequence(NucleotideSequence):

	@validator("sequence")
	@classmethod
	def validate_sequence(cls, value: str):
		value = value.upper()

		if not all(i in ["A", "T", "C", "G"] for i in value):
			raise ValueError("Not allowed characters in sequence")

		cls.sequence_type = "DNA"
		return value

	def transcribe(self):
		return self.sequence.replace("T", "U")

	def complement(self):
		mapping = {"A": "T", "T": "A", "G": "C", "C": "G"}
		return ''.join([mapping[i] for i in self.sequence])

	def translate(self) -> str:
		"""
		Maps given RNA sequence to protein short form.

		Returns:
			str: Protein sequence
		"""
		mapping = {'M': ['AUG'],
		           'W': ['UGG'],
		           'Y': ['UAU', 'UAC'],
		           'F': ['UUU', 'UUC'],
		           'C': ['UGU', 'UGC'],
		           'N': ['AAU', 'AAC'],
		           'D': ['GAU', 'GAC'],
		           'Q': ['CAA', 'CAG'],
		           'E': ['GAA', 'GAG'],
		           'H': ['CAU', 'CAC'],
		           'K': ['AAA', 'AAG'],
		           'I': ['AUU', 'AUC', 'AUA'],
		           'G': ['GGU', 'GGC', 'GGA', 'GGG'],
		           'A': ['GCU', 'GCC', 'GCA', 'GCG'],
		           'V': ['GUU', 'GUC', 'GUA', 'GUG'],
		           'T': ['ACU', 'ACC', 'ACA', 'ACG'],
		           'P': ['CCU', 'CCC', 'CCA', 'CCG'],
		           'L': ['CUU', 'CUC', 'CUA', 'CUG', 'UUA', 'UUG'],
		           'S': ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC'],
		           'R': ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'],
		           '<': ['UAA', 'UAG', 'UGA']}

		rna_sequence = self.transcribe()

		# Quality check
		if self.length % 3:
			raise ValueError("Sequence must be divisible by 3!")

		# Split into pairs of 3
		codons = [rna_sequence[i:i + 3] for i in range(0, self.length, 3)]

		protein = ''.join([k for x in codons for k, v in mapping.items() if x in v])

		return ''.join([">" if not nr and i == "M" else i for nr, i in enumerate(protein)])


class RnaSequence(NucleotideSequence):

	@validator("sequence")
	@classmethod
	def validate_sequence(cls, value: str):
		value = value.upper()

		if not all(i in ["A", "U", "C", "G"] for i in value):
			raise ValueError("Not allowed characters in sequence")

		cls.sequence_type = "RNA"
		return value

	def transcribe(self):
		return self.sequence.replace("U", "T")

	def complement(self):
		mapping = {"A": "U", "U": "A", "G": "C", "C": "G"}
		return ''.join([mapping[i] for i in self.sequence])

	def translate(self) -> str:
		"""
		Maps given RNA sequence to protein short form.

		Returns:
			str: Protein sequence
		"""
		mapping = {'M': ['AUG'],
		           'W': ['UGG'],
		           'Y': ['UAU', 'UAC'],
		           'F': ['UUU', 'UUC'],
		           'C': ['UGU', 'UGC'],
		           'N': ['AAU', 'AAC'],
		           'D': ['GAU', 'GAC'],
		           'Q': ['CAA', 'CAG'],
		           'E': ['GAA', 'GAG'],
		           'H': ['CAU', 'CAC'],
		           'K': ['AAA', 'AAG'],
		           'I': ['AUU', 'AUC', 'AUA'],
		           'G': ['GGU', 'GGC', 'GGA', 'GGG'],
		           'A': ['GCU', 'GCC', 'GCA', 'GCG'],
		           'V': ['GUU', 'GUC', 'GUA', 'GUG'],
		           'T': ['ACU', 'ACC', 'ACA', 'ACG'],
		           'P': ['CCU', 'CCC', 'CCA', 'CCG'],
		           'L': ['CUU', 'CUC', 'CUA', 'CUG', 'UUA', 'UUG'],
		           'S': ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC'],
		           'R': ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'],
		           '<': ['UAA', 'UAG', 'UGA']}

		# Quality check
		if self.length % 3:
			raise ValueError("Sequence must be divisible by 3!")

		# Split into pairs of 3
		codons = [self.sequence[i:i + 3] for i in range(0, self.length, 3)]

		protein = ''.join([k for x in codons for k, v in mapping.items() if x in v])

		return ''.join([">" if not nr and i == "M" else i for nr, i in enumerate(protein)])