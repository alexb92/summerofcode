import argparse
from nucleotide_models import DnaSequence, RnaSequence # the CLI tool needs a different way of importing


def create_parser():
	parser = argparse.ArgumentParser(prog='nseq', description='CLI-wrapper for our REST API models')

	parser.add_argument('sequence', metavar='seq', type=str, help='a nucleotide sequence')
	parser.add_argument('-a', '--analyze', dest='analyze', action='store_true', help='show main stats of sequence')
	parser.add_argument('-l', '--length', dest='length', action='store_true', help='show length of sequence')
	parser.add_argument('-m', '--melt', dest='melt', action='store_true', help='show melting temperature of sequence')
	parser.add_argument('-g', '--gc', dest='gc', action='store_true', help='show gc content of sequence (in percent)')
	parser.add_argument('-c', '--complement', action='store_true', help='generate complementary sequence')
	parser.add_argument('-t', '--transcribe', action='store_true', help='transcribe sequence')
	parser.add_argument('-p', '--translate', action='store_true', help='translate sequence')
	parser.add_argument('-n', '--ncount', action='store_true', help='show count of each individual nucleotide')
	parser.add_argument('-d', '--mutations', dest='mutated_sequence', type=str, help='show amount of point mutations between sequences')
	parser.add_argument('--test', type=int, choices=[1, 2, 3])
	return parser

def main(parser: argparse.ArgumentParser):
	args = parser.parse_args()
	n_type = DnaSequence if "T" in args.sequence.upper() else RnaSequence
	seq = n_type(sequence=args.sequence)

	if args.analyze:
		print("Analysis:")
		for k, v in seq.analyze().items():
			print(f"\t{k}: {v}")
			print("")
	if args.length:
		print(f"Length: {seq.length}", end="\n\n")
	if args.melt:
		print(f"Melting Temperature: {seq.melting_temperature}", end="\n\n")
	if args.gc:
		print(f"GC-Content: {seq.gc_content}", end="\n\n")
	if args.complement:
		print(f"Complementary Sequence: {seq.complement()}", end="\n\n")
	if args.transcribe:
		print(f"Transcribed Sequence: {seq.transcribe()}", end="\n\n")
	if args.translate:
		print(f"Translated Sequence: {seq.translate()}", end="\n\n")
	if args.ncount:
		print(f"Nucleotide Count: {seq.count_nucleotides()}", end="\n\n")
	if args.mutated_sequence:
		print(f"Amount of Point Mutations: {seq.count_point_mutations(n_type(sequence=args.mutated_sequence))}")


if __name__ == '__main__':
	parser = create_parser()
	main(parser)

