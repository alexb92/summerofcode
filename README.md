# Summer of Code
### Learning Python by REST API example

## Introduction
This course aims to rehearse and improve your knowledge of Python. While it is required to have at least
a little previous experience, the overall learning curve is intentionally kept not too steep.

The main topic is the creation of a REST API. The end goal, however, is not to create a production ready API but rather
to have used most of the core principles and methods common to programming and Python in particular.

## Workflow
This course is heavily dependent on Git and common workflows that it offers. Every new lesson will be distributed as
a new release, including proper versioning. 

All the work that is done until a lesson is published will be under a separate branch. Once this work is complete, a
pull request will be created and the branch will be merged into `main`. The development branch will be deleted 
on merging.

**Topics that will be covered:**
* Lesson 1
    * Data types
    * Basic logic
    * General coding do's and don'ts
    * What is a REST API / fastapi
    

* Lesson 2
    * Shortcuts & Advanced features
    * Git & workflows
    * REST API
    

* Lesson 3
    * OOP
    * Dunder methods
    * Decorators
    * Wrappers
    * Exercises
    

* Lesson 4
    * Code structure
    * Exception handling & debugging
    * Typing & dataclasses
    * REST API


* Lesson 5
    * API models
    * Pydantic
    * REST API


* Lesson 6
    * Argparse module
    * Client generation & simulating traffic
  
## Installation
The required packages for this repository can be downloaded by either using the provided pipfile or requirements.txt.

### Pipenv
When creating a new pipenv, check the box "Install packages from Pipfile". If you missed this opportunity, simply
use the terminal of your IDE and run `pipenv update`

### Virtualenv
Run the following code in your terminal: `pip install -r requirements.txt`

### Conda/ Anaconda/ etc.
You're on your own buddy ;)

### System interpreter
Don't. Just don't.

## Running Jupyter Notebook
### Pipenv
Use the following command: `pipenv run jupyter notebook`

### Virtualenv
Use the following command: `jupyter notebook`

### Conda/ Anaconda/ etc.
Use the following command: `jupyter notebook` (I guess)

## Running Fastapi
This command should be working for all environments: `uvicorn main:app --reload`
(if it doesn't, Google is your friend and overlord ;) )

The "reload" flag automatically restarts the server whenever the code changes. This way, your changes will always be
reflected immediately.

Once the application is started, you can navigate to:
* [User interface](http://localhost:8000/docs)
* [Documentation](http://localhost:8000/redoc)

## Generating a REST API client library
**REQUIRES DOCKER!**

To generate a client library for a fastapi application, simply start the fastapi server, navigate to the user interface
and click on the openapi.json link (directly under the fastapi logo in the top left). This will open another browser 
tab containing the specifications to your API in the OpenAPI format.

Copy & Paste everything into an empty .json file called openapi.json. Open a terminal and navigate to the directory
said file is in and run the respective code for your OS from below.

### Linux: 
```Shell
docker run --rm -v "${PWD}:/local" docker.io/openapitools/openapi-generator-cli:latest generate \
-i /local/openapi.json \
-g python-legacy \
--additional-properties=packageName=soc_client \
-o /local/gen_out
```

### Windows:
```Shell
docker run --rm -v "%CD%:/local" docker.io/openapitools/openapi-generator-cli:latest generate ^
-i /local/openapi.json ^
-g python-legacy ^
--additional-properties=packageName=soc_client ^
-o /local/gen_out
```

When the generator has finished, you can remove the image with the following command:

```Shell
docker rmi openapitools/openapi-generator-cli
```

The output folder contains a folder called soc_client, within are the Python bindings for your API. Cut & paste
the folder to the top-level of your project. From there, you can import all objects into your scripts. The 
remains of the output folder can be discarded.


## Resources
* [Python Documentation](https://docs.python.org/3/)
* [Python 101](https://python101.pythonlibrary.org/)
* [Python Style Guide](https://pep8.org/)
* [Dunder Methods](https://holycoders.com/python-dunder-special-methods/)
* [Type Hints](https://docs.python.org/3/library/typing.html)
* [Data Classes](https://docs.python.org/3/library/dataclasses.html)
* [Pydantic](https://pydantic-docs.helpmanual.io/)
* [Exceptions](https://docs.python.org/3/library/exceptions.html)
* [GIT Basics](https://git-scm.com/book/en/v2)
* [REST API](https://en.wikipedia.org/wiki/Representational_state_transfer)
* [Fastapi Documentation](https://fastapi.tiangolo.com/)
* [OpenAPI](https://en.wikipedia.org/wiki/OpenAPI_Specification)
* [argparse](https://docs.python.org/3/howto/argparse.html)  

### Further reading:
* [Python async](https://realpython.com/python-async-features/)
* [MongoDB](https://www.mongodb.com/)
* [Docker Basics](https://www.docker.com/101-tutorial)
* [Docker-Compose](https://docs.docker.com/compose/gettingstarted/)
